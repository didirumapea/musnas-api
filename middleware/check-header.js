// const jwt = require('jsonwebtoken')
// const config = require('../config')
const XENDIT_TOKEN = process.env.NODE_ENV === 'production' ? process.env.XENDIT_CALLBACK_TOKEN_LIVE : process.env.XENDIT_CALLBACK_TOKEN_TEST

module.exports = (req, res, next) => {
    // console.log(req.headers)
    try{
        if (req.headers['x-callback-token'] === XENDIT_TOKEN){
            next()
        }else{
            if (req.headers['on-project'] !== process.env.H_ON_PROJECT){
                // console.log('pc')
                return res.status(401).json({
                    // message: 'auth Failed'
                    message: 'You are not allowed here'
                })
            }else if(req.headers['signature-key'] !== process.env.H_SIGN_KEY){
                return res.status(401).json({
                    // message: 'auth Failed'
                    message: 'You are in restristic area.'
                })
            }
            else{
                next()
            }
        }

    }catch (error) {
        let err = null
        if (error.message === 'jwt expired'){
            err = 'Token Expired'
        }else{
            err = 'Auth Failed'
        }
        return res.status(401).json({
            // message: 'auth Failed'
            message: err
        })
    }
}