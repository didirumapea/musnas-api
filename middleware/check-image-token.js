const jwt = require('jsonwebtoken')
const config = require('../config')
// const XENDIT_TOKEN = process.env.NODE_ENV === 'production' ? process.env.XENDIT_CALLBACK_TOKEN_LIVE : process.env.XENDIT_CALLBACK_TOKEN_TEST

module.exports = (req, res, next) => {
    // console.log(req.headers)
    try{
        const token = req.headers['x-image-token']
        req.userData = jwt.verify(token, config.jwtSecretKeyImagesToken);
        next();
    }catch (error) {
        let err = null
        if (error.message === 'jwt expired'){
            err = 'Image Token Expired'
        }else{
            err = 'Image Token Auth Failed'
        }
        return res.status(401).json({
            // message: 'auth Failed'
            message: err
        })
    }
}