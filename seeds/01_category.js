// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('category').del()
    .then(function () {
      // Inserts seed entries
      return knex('category').insert([
        {
            id: 1,
            name: 'Paud/TK',
            price: 2000,
        },
        {
            id: 2,
            name: 'SD',
            price: 2000,
        },
        {
            id: 3,
            name: 'SMP',
            price: 2000,
        },
          {
              id: 4,
              name: 'SMA',
              price: 2000,
          },
          {
              id: 5,
              name: 'Mahasiswa',
              price: 5000,
          },
          {
              id: 6,
              name: 'Dewasa',
              price: 5000,
          },
      ]);
    }).catch((err) => {
        console.log(err)
      });
};
