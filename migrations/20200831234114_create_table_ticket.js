
exports.up = function(knex, Promise) {
    return knex.schema.createTable('ticket', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.bigInteger('ticket_no')
            .notNullable()
            .unsigned()
            .index();
        table.bigInteger('order_no')
            .unsigned()
            .notNullable()
            .references('order_no')
            .inTable('order')
            .onDelete('CASCADE')
            .index();
        table.integer('category_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('category')
            .onDelete('CASCADE')
            .index();
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0)
        table.specificType('is_active', 'tinyint(1)')
            .defaultTo(0)
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('ticket')
};
