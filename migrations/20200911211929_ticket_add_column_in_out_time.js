
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('ticket', (table) => {
        table.dateTime('in_time')
            .after('category_id')
            .defaultTo(null);
        table.dateTime('out_time')
            .after('in_time')
            .defaultTo(null);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable("ticket", (table) => {
        table.dropColumn('in_time');
        table.dropColumn('out_time');
    })
};
