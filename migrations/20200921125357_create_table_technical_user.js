
exports.up = function(knex, Promise) {
    return knex.schema.createTable('technician_user', (table) => {
        table.increments()
        table.integer('account_type_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('account_type')
            .onDelete('CASCADE')
            .index();
        table.string('nik').notNullable();
        table.string('nip').notNullable();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('phone').notNullable();
        table.text('address').notNullable();
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0)
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('technician_user')
};
