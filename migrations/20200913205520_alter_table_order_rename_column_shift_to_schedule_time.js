
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.renameColumn('shift', 'schedule_time')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.renameColumn('schedule_time', 'shift')
    })
};
