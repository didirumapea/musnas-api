
exports.up = function(knex, Promise) {
    return knex.schema.createTable('order', (table) => {
        // increment is auto added unsigned
        table.increments()
        table.bigInteger('invoice_no')
            .notNullable()
            .unsigned()
            .unique()
            .index();
        table.bigInteger('order_no')
            .notNullable()
            .unsigned()
            .unique()
            .index();
        table.string('name')
        table.string('email')
        table.string('ticket_type')
        table.string('citizenship')
        table.string('city')
        table.string('agency')
        table.enum('payment_status', ['pending', 'paid', 'expired'])
            .notNullable()
            .defaultTo('pending');
        table.specificType('is_deleted', 'tinyint(1)')
            .defaultTo(0)
        table.specificType('is_active', 'tinyint(1)')
            .defaultTo(0)
        table.date('schedule_date')
        table.time('shift')
        table.dateTime('in_time')
        table.dateTime('out_time')
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('order')
};
