
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('category', (table) => {
        table.integer('ticket_type_id')
            .unsigned()
            .notNullable()
            .defaultTo(1)
            .references('id')
            .inTable('ticket_type')
            .onDelete('CASCADE')
            .index()
            .after('id');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('ticket_type_id', (table) => {
        table.dropColumn('ticket_type_id');
    })
};
