
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('ticket', (table) => {
        table.string('ticket_no')
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable("ticket", (table) => {
        table.bigInteger('ticket_no')
            .alter();
    })
};
