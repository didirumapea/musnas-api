
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.integer('country_id')
            .unsigned()
            .notNullable()
            .references('country_id')
            .inTable('countrytranslation')
            .onDelete('CASCADE')
            .index()
            .after('ticket_type')
            .defaultTo(99);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.dropColumn('country_id');
    })
};
