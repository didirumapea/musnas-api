
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.string('payment_url')
            .after('agency')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.dropColumn('payment_url');
    })
};
