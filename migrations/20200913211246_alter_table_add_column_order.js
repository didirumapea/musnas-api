
exports.up = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.string('booking_no')
            .unique()
            .after('invoice_no')
            .defaultTo(null);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('order', (table) => {
        table.dropColumn('booking_no');
    })
};
