const basepath =  __dirname.replace('plugins', '');
const moment = require('moment');
const cfg = require('../config');
const fs = require('fs');
const mkdirp = require('mkdirp');
// require('core-js/modules/es.promise');
// require('core-js/modules/es.object.assign');
// require('core-js/modules/es.object.keys');
// require('regenerator-runtime/runtime');
// const ExcelJS = require('exceljs/dist/es5');
// polyfills required by exceljs
const ExcelJS = require('exceljs'); // problem server shuould kill with `killall node`
// console.log(basepath+'public/templates')
// #region GENERATE PROCESS REKONSILE MERCHANT VOUCHER CODE

// SEND WITH GOOGLE ACCOUNT
exports.orderGenerateXlsx = async (data, res) => {
    // var Excel = require('exceljs');
    // console.log(data)
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `order-${dateNow}.xlsx`
    let filepath = cfg.assetPath + 'musnas-files/assets/files/order/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR';
    workbook.lastModifiedBy = 'DnN';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    // MERGE CELLS
    worksheet.mergeCells('A1:A2');
    worksheet.mergeCells('B1:B2');
    worksheet.mergeCells('C1:C2');
    worksheet.mergeCells('D1:J1');
    worksheet.mergeCells('K1:K2');
    // NAMING HEADER CELLS
    worksheet.getCell('A1').value = 'No';
    worksheet.getCell('B1').value = 'Nama/Instansi (Name/Institution)';
    worksheet.getCell('C1').value = 'Alamat/Nationality';
    worksheet.getCell('D1').value = 'Jumlah Pengunjung (Total Visitor)';
    worksheet.getCell('D2').value = 'PAUD/TK';
    worksheet.getCell('E2').value = 'SD';
    worksheet.getCell('F2').value = 'SMP';
    worksheet.getCell('G2').value = 'SMA';
    worksheet.getCell('H2').value = 'Mhs.';
    worksheet.getCell('I2').value = 'Dewasa';
    worksheet.getCell('J2').value = 'Guest';
    worksheet.getCell('K2').value = 'Keterangan';
    // ALIGNMENT CELLS
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;
    worksheet.getCell('B1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;
    worksheet.getCell('C1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;
    worksheet.getCell('D1').alignment = { horizontal: 'center', wrapText: true} ;
    worksheet.getCell('K1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;

    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };

    // worksheet.columns = [
    //     {header: 'No', key: 'no', width: 8},
    //     {header: 'Nama/Instansi (Name/Institution', key: 'no', width: 20},
    //     {header: 'Alamat/Nationality', key: 'no', width: 18},
    //     // {header: 'Email', key: 'email', width: 32},
    //     // {header: 'Name', key: 'name', width: 32},
    //     // {header: 'Phone', key: 'phone', width: 10, outlineLevel: 1},
    //     // {header: 'Age', key: 'age', width: 15},
    //     // {header: 'Caption', key: 'caption', width: 15},
    // ];

    worksheet.columns = [
        {key: 'no', width: 8},
        {key: 'agency', width: 20},
        {key: 'city', width: 18},
        {key: 'Paud/TK', width: 8},
        {key: 'SD', width: 8},
        {key: 'SMP', width: 8},
        {key: 'SMA', width: 8},
        {key: 'Mahasiswa', width: 8},
        {key: 'Dewasa', width: 8},
        {key: 'guest', width: 8},
        {key: 'keterangan', width: 18},
        {key: 'keterangan', width: 18},
        // {header: 'Email', key: 'email', width: 32},
        // {header: 'Name', key: 'name', width: 32},
        // {header: 'Phone', key: 'phone', width: 10, outlineLevel: 1},
        // {header: 'Age', key: 'age', width: 15},
        // {header: 'Caption', key: 'caption', width: 15},
    ];
    // console.log(data)
    data.forEach((element, i) => {
        worksheet.addRow(
            {
                no: i + 1,
                agency: element.agency,
                city: element.city,
                'Paud/TK': element['Paud/TK'] === undefined ? 0 : element['Paud/TK'],
                SD: element.SD === undefined ? 0 : element.SD,
                SMP: element.SMP === undefined ? 0 : element.SMP,
                SMA: element.SMA === undefined ? 0 : element.SMA,
                Mahasiswa: element.Mahasiswa === undefined ? 0 : element.Mahasiswa,
                Dewasa: element.Dewasa === undefined ? 0 : element.Dewasa,
            })
    });
    // BORDER CELLS HEADER
    const alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K']
    for (let j = 1; j < 3; j++){
        for (let x = 0; x < alpha.length; x++){
            worksheet.getCell(alpha[x]+j).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
        }
    }
    // BORDER CELLS DATA
    for (let row = 3; row < data.length+3; row++){
        for (let column = 0; column < alpha.length; column++){
            worksheet.getCell(alpha[column]+row).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
        }
    }




    await workbook.xlsx.writeFile(filepath + filename)
        .then(function () {
            // done
            // console.log('done generate list pre register user')
            // SendEmail(filename)
            res.json({
                success: true,
                msg: 'Generate list ticket success!',
                count: data.length,
                // data: data,
                path: filename
            })
        })
        .catch(err => {
            console.log(err)
            res.json({
                success: false,
                // msg: 'Generate list pre register user success!',
                // count: data.length,
                // // data: data,
                // path: filename
            })
        });
}

exports.TicketGenerateXlsx = async (data, res) => {
    // var Excel = require('exceljs');
    // console.log(data)
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `ticket-${dateNow}.xlsx`
    let filepath = cfg.assetPath + 'musnas/assets/files/ticket/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR';
    workbook.lastModifiedBy = 'DnN';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    // MERGE CELLS
    worksheet.mergeCells('A1:A2');
    worksheet.mergeCells('B1:B2');
    worksheet.mergeCells('C1:C2');
    worksheet.mergeCells('D1:K1');
    worksheet.mergeCells('L1:L2');
    // NAMING HEADER CELLS
    worksheet.getCell('A1').value = 'No';
    worksheet.getCell('B1').value = 'Nama/Instansi (Name/Institution)';
    worksheet.getCell('C1').value = 'Alamat/Nationality';
    worksheet.getCell('D1').value = 'Jumlah Pengunjung (Total Visitor)';
    worksheet.getCell('D2').value = 'PAUD/TK';
    worksheet.getCell('E2').value = 'SD';
    worksheet.getCell('F2').value = 'SMP';
    worksheet.getCell('G2').value = 'SMA';
    worksheet.getCell('H2').value = 'SMA';
    worksheet.getCell('I2').value = 'Mhs.';
    worksheet.getCell('J2').value = 'Dewasa';
    worksheet.getCell('K2').value = 'Guest';
    worksheet.getCell('L2').value = 'Keterangan';
    // ALIGNMENT CELLS
    worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;
    worksheet.getCell('B1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;
    worksheet.getCell('C1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;
    worksheet.getCell('D1').alignment = { horizontal: 'center', wrapText: true} ;
    worksheet.getCell('L1').alignment = { vertical: 'middle', horizontal: 'center', wrapText: true} ;

    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };

    // worksheet.columns = [
    //     {header: 'No', key: 'no', width: 8},
    //     {header: 'Nama/Instansi (Name/Institution', key: 'no', width: 20},
    //     {header: 'Alamat/Nationality', key: 'no', width: 18},
    //     // {header: 'Email', key: 'email', width: 32},
    //     // {header: 'Name', key: 'name', width: 32},
    //     // {header: 'Phone', key: 'phone', width: 10, outlineLevel: 1},
    //     // {header: 'Age', key: 'age', width: 15},
    //     // {header: 'Caption', key: 'caption', width: 15},
    // ];

    worksheet.columns = [
        {key: 'no', width: 8},
        {key: 'nox', width: 20},
        {key: 'noxx', width: 18},
        {key: 'no1', width: 8},
        {key: 'no2', width: 8},
        {key: 'no3', width: 8},
        {key: 'no4', width: 8},
        {key: 'no5', width: 8},
        {key: 'no6', width: 8},
        {key: 'no7', width: 8},
        {key: 'no8', width: 8},
        {key: 'no9', width: 18},
        // {header: 'Email', key: 'email', width: 32},
        // {header: 'Name', key: 'name', width: 32},
        // {header: 'Phone', key: 'phone', width: 10, outlineLevel: 1},
        // {header: 'Age', key: 'age', width: 15},
        // {header: 'Caption', key: 'caption', width: 15},
    ];
    console.log(data.length)
    data.forEach((element, i) => {
        worksheet.addRow(
            {
                no: i + 1,
                nox: i + 1,
                noxx: i + 1,
                email: element.email,
                name: element.name,
                phone: element.phone,
                age: element.age,
                caption: element.caption,
            })
    });
    // BORDER CELLS HEADER
    const alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']
    for (let j = 1; j < 3; j++){
        for (let x = 0; x < alpha.length; x++){
            worksheet.getCell(alpha[x]+j).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
        }
    }
    // BORDER CELLS DATA
    for (let row = 3; row < data.length+3; row++){
        for (let column = 0; column < alpha.length; column++){
            worksheet.getCell(alpha[column]+row).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
        }
    }




    await workbook.xlsx.writeFile(filepath + filename)
        .then(function () {
            // done
            // console.log('done generate list pre register user')
            // SendEmail(filename)
            res.json({
                success: true,
                msg: 'Generate list ticket success!',
                count: data.length,
                // data: data,
                path: filename
            })
        })
        .catch(err => {
            console.log(err)
            res.json({
                success: false,
                // msg: 'Generate list pre register user success!',
                // count: data.length,
                // // data: data,
                // path: filename
            })
        });
}

exports.disasterMappingGenerateXlsx = async (data, res) => {
    // var Excel = require('exceljs');
    // console.log(data)
    let dateNow = moment().format("DD-MM-YYYY")
    let filename = `disaster-mapping-${dateNow}.xlsx`
    let filepath = cfg.assetPath + 'bumikita-files/assets/files/disaster-mapping/'
    if (!fs.existsSync(filepath)){
        console.log('file not exist and will be created...')
        const made = mkdirp.sync(filepath)
        console.log(`made directories, starting with ${made}`)
        console.log('done create directory')
    }
    // define xlsx
    let workbook = new ExcelJS.Workbook();
    workbook.creator = 'DKR';
    workbook.lastModifiedBy = 'Anony';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;
    // this for opened the xlsx
    workbook.views = [
        {
            x: 0, y: 0, width: 20000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ]
    // create worksheet
    let worksheet = workbook.addWorksheet('My Sheet');
    //   worksheet.getCell('A1').dataValidation = {
    //     type: 'whole',
    //     operator: 'notEqual',
    //     showErrorMessage: true,
    //     formulae: [5],
    //     errorStyle: 'error',
    //     errorTitle: 'Five',
    //     error: 'The value must not be Five'
    // };

    worksheet.columns = [
        {header: 'No', key: 'no', width: 8},
        {header: 'User', key: 'name', width: 32},
        {header: 'Title', key: 'title', width: 32},
        {header: 'Subtitle', key: 'subtitle', width: 10, outlineLevel: 1},
        {header: 'Category', key: 'category', width: 15},
        {header: 'Description', key: 'description', width: 15},
        {header: 'Type', key: 'type', width: 15},
        {header: 'Needs', key: 'needs', width: 15},
        {header: 'Access to location', key: 'access_to_location', width: 15},
        // {header: 'Coordinate', key: 'coordinate', width: 15},
        {header: 'Lat', key: 'lat', width: 15},
        {header: 'Lng', key: 'lng', width: 15},
        {header: 'Created At', key: 'created_at', width: 15},
    ];

    data.forEach((element, i) => {
        worksheet.addRow(
            {
                no: i + 1,
                name: element.name,
                title: element.title,
                subtitle: element.subtitle,
                category: element.category,
                description: element.description,
                type: element.type,
                needs: element.needs,
                access_to_location: element.access_to_location,
                // coordinate: element.coordinate,
                lat: element.coordinate.split('@')[0],
                lng: element.coordinate.split('@')[1],
                created_at: element.created_at,
            });
        // console.log(element.merchant)
        // console.log(i)
    });

    await workbook.xlsx.writeFile(filepath + filename)
        .then(function () {
            // done
            // console.log('done generate list pre register user')
            // SendEmail(filename)
            res.json({
                success: true,
                msg: 'Generate list disaster mapping success!',
                total_data: data.length,
                // data: data,
                path: filename
            })
        })
        .catch(err => {
            console.log(err)
            res.json({
                success: false,
                // msg: 'Generate list pre register user success!',
                // count: data.length,
                // // data: data,
                // path: filename
            })
        });
}

