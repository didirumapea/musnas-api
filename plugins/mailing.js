const nodemailer = require('nodemailer');
const handleBars = require('nodemailer-express-handlebars');
const basepath =  __dirname.replace('plugins', '');
const currencyFormat = require('../plugins/currency-format')
const moment = require('moment');
const cfg = require('../config');

//region SEND EMAIL PEPIPOST

// ---- RESET PASSWORD
exports.sendEmailResetPassword = (mData) => {
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '25', // '587', //'465',
        secureConnection: true, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'bumikitaadmiin',
            pass: 'bumikitaadmiin_e6f143be75d58fa9465dfb43df02f44f'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mData.template+'.htm',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        // from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        from: 'Bumikita Reset Password'+' <security@bumikita.or.id>', // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.to,
        subject: mData.subject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": 'BMKT'+moment().format('X'),
            },
        template: mData.template, // html body | file name
        context: {
            username : mData.to,
            password: mData.new_pass,
            fullname: mData.name
        },
        // attachments: [
        //   {   // filename and content type is derived from path
        //   path: cfg.assetPath+'receipt-voucher-pdf/'+mData.inv_code+'.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};

// ---- CONTENT ADMIN NOTIFICATION
exports.sendEmailPepipost = (mData) => {
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', // '587', //'465', '25'
        secureConnection: false, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'bumikitaadmiin',
            pass: 'bumikitaadmiin_e6f143be75d58fa9465dfb43df02f44f'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.htm',
            partialsDir: basepath+'public/templates/bumikita',
            layoutsDir: basepath+'public/templates/bumikita',
            defaultLayout: mData.template+'.htm',
        },
        viewPath: basepath+'public/templates/bumikita', // folder name
        extName: '.htm'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        // from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        from: `${mData.from_aliases} <${mData.from}>`, // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.to,
        subject: mData.subject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": 'BMKT'+moment().format('X'),
            },
        template: mData.template, // html body | file name
        context: mData.context
        // context: {
        //     title : mData.title,
        //     user: mData.user,
        // },
        // attachments: [
        //   {   // filename and content type is derived from path
        //   path: cfg.assetPath+'receipt-voucher-pdf/'+mData.inv_code+'.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};

// ---- SEND EMAIL EMS
exports.sendEmail = (mData) => {
    let transporter = nodemailer.createTransport({
        pool: true,
        maxMessages: 100000,
        rateDelta: 2000,
        rateLimit: 2000,
        host: 'smtp.pepipost.com', //'202.158.93.230',//'smtp.pepipost.com', //'smtp.zoho.com', //'163.53.192.219', //'163.53.192.219',
        port: '587', // '587', //'465', '25'
        secureConnection: false, // true for 465, false for other ports
        secure: false,
        auth: {
            user: 'dev0543gd',
            pass: 'dev0543gd_a15ef9906769080e7d36d6d5a03a0a20'
        }
    });

    let handlebarOptions = {
        viewEngine: {
            extName: '.html',
            partialsDir: basepath+'public/templates',
            layoutsDir: basepath+'public/templates',
            defaultLayout: mData.template+'.html',
        },
        viewPath: basepath+'public/templates', // folder name
        extName: '.html'
    };

    transporter.use('compile', handleBars(handlebarOptions
        // viewPath: basepath+'public/templates', // folder name
        // //extName: '.html'
        // extName: '.htm'
    ));
    let mailOptions = {
        // from: 'Rimember Reset Password'+' <resetpwd@rimember.id>', // sender address ex : <test-sendemail@intercity.com>
        from: `${mData.from_aliases} <${mData.from}>`, // sender address ex : <test-sendemail@intercity.com>
        //from: '"Bukopin Card Center" <test-sendemail@intercity.com>', // sender address
        to: mData.to,
        subject: mData.subject, // Subject line
        priority : 'high',
        important: true,
        headers:
            {
                "x-apiheader": 'MUSNAS'+moment().format('X'),
                // "x-apiheader": `MUSNAS@123@321@$789`,
            },
        template: mData.template, // html body | file name
        context: mData.context,
        // context: {
        //     title : mData.title,
        //     user: mData.user,
        // },
        // attachments: [
        //   {   // filename and content type is derived from path
        //       filename: 'BM010820_000001.pdf',
        //       path: 'https://cdnx.bumikita.or.id/assets/files/pdf/CYC010820/BM010820_000001.pdf'
        //     // path: cfg.cdn+'files/pdf/cyc010820/BM010820_000001.pdf'
        //   }
        // ]
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            //return console.log(error);
            console.log(error);
            // res.json({
            //   success: true,
            //   data: 'send email error'
            // });

        } else {
            console.log(info);
            // res.json({
            //   success: true,
            //   data: 'send email success'
            // });
        }
    })
};