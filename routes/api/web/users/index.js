const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const randomstring = require("randomstring");
const Xendit = require('xendit-node');
const mailing = require('../../../../plugins/mailing')
// let secret_key = 'xnd_development_DiNPImnSggXO94ts2A8Ud8IqDtRoyzPxytkip5kmlmCchRtJmyAWtzV6hBb7T' // Dev
// if (process.env.NODE_ENV === 'production'){
//     secret_key = 'xnd_production_6YKZaM4xMAI0xONEg6h72OfkelfJcrSOQNY1jdUA3SjMi35TfQrQn5DnMftt4D' // live
// }

// POST ORDER
router.post('/order', (req, res) => {
    const x = new Xendit({
        secretKey: process.env.NODE_ENV === 'production' ? process.env.XENDIT_SECRET_KEY_LIVE : process.env.XENDIT_SECRET_KEY_DEV,
    });
    const { Invoice } = x;
    const i = new Invoice({});

    db.select(
        'id'
    )
        .from('order')
        .where('booking_no', req.body.booking_no)
        .then((data) => {
            if (data.length !== 0){
                return res.json({
                    success: false,
                    message: 'Booking no already used',
                })
            }else{
                (async function() {
                    try {
                        let invoice = await i.createInvoice({
                            externalID: req.body.order_no,
                            payerEmail: req.body.email,
                            description: 'Invoice for Museum Nasional Ticket',
                            amount: req.body.price,
                        });
                        console.log(invoice.invoice_url)
                        let ticket = JSON.stringify(req.body.list_ticket) // FOR LIVE
                        if (req.headers['x-post-by'] === 'postman'){
                            ticket = req.body.list_ticket // USING POSTMAN
                        }
                        // req.body.order_no = '' + ticketTypeId(req.body.ticket_type) + moment().unix() + randomNumber(5)
                        // req.body.invoice_no = '' + moment().unix() + randomNumber(7)
                        delete req.body.list_ticket
                        delete req.body.price
                        let no_queue = 0
                        // console.log(ticket)
                        req.body.payment_url = invoice.invoice_url
                        await db('order')
                            .insert(req.body)
                            .then(data_order => {
                                let objQuery = []
                                // Generating Ticket
                                JSON.parse(ticket).forEach(element => {
                                    for (let x = 0; x < element.jml_tiket; x++){
                                        no_queue++
                                        objQuery.push({
                                            ticket_no: req.body.order_no + no_queue,
                                            order_no: req.body.order_no,
                                            category_id: element.category_id
                                        })
                                    }
                                })
                                // INSERT TICKET
                                no_queue = 0
                                db('ticket')
                                    .insert(objQuery)
                                    .then(data => {
                                        res.json({
                                            success: true,
                                            message: "Add ticket & order success",
                                            // count: result.length,
                                            data_ticket: data,
                                            data_order: data_order,
                                            xendit_url: invoice.invoice_url,
                                            order_no: req.body.order_no
                                        });
                                        mailing.sendEmail({
                                            template: 'payment',
                                            to: req.body.email,
                                            // to: ['emiriafikiputri@gmail.com', 'didirumapea@gmail.com'],
                                            // to: 'didirumapea@gmail.com',
                                            from_aliases: 'Museum Nasional Indonesia',
                                            from: 'info@museumnasional.or.id',
                                            subject: `New invoice Pending from Museum Nasional Indonesia`,
                                            context: {
                                                // title: title,
                                                name: req.body.name,
                                                // module_id: module_id,
                                                inv_url: process.env.WEB_URL,
                                                ext_id: req.body.order_no
                                            }
                                        })
                                    })

                            })
                            .catch((err) => {
                                console.log(err)
                                res.json({
                                    success: false,
                                    message: "Order failed.",
                                    // count: data.length,
                                    data: err,
                                });
                            });
                        // console.log('created invoice', invoice); // eslint-disable-line no-console
                        // return invoice
                        // const retrievedInvoice = await i.getInvoice({ invoiceID: invoice.id });
                        // // eslint-disable-next-line no-console
                        // console.log('retrieved invoice', retrievedInvoice);
                        //
                        // const expiredInvoice = await i.expireInvoice({
                        //     invoiceID: retrievedInvoice.id,
                        // });
                        // // eslint-disable-next-line no-console
                        // console.log('expired invoice', expiredInvoice);
                        //
                        // const invoices = await i.getAllInvoices();
                        // // eslint-disable-next-line no-console
                        // console.log('first 10 invoices', invoices);

                        // process.exit(0);
                    } catch (e) {
                        console.error(e); // eslint-disable-line no-console
                        process.exit(1);
                    }
                })();
            }
        })
});

// GET ORDER BY ID
router.get('/order-no=:order_no',  (req, res) => {


    db.select(
        '*'
    )
        .from('order as t1')
        .where('order_no', req.params.order_no)
        .then(data => {
            // console.log(data)
                db.select(
                    't2.name',
                    't2.price as rate',
                )
                    .count('* as total')
                    .sum('t2.price as total_price')
                    .from('ticket as t1')
                    .innerJoin('category as t2', 't1.category_id', 't2.id')
                    .where('order_no', req.params.order_no)
                    .groupBy('t1.category_id')
                    .then(data2 => {
                        res.json({
                            success: true,
                            message: "Get order detail by order no success",
                            // sortBy: sortBy,
                            data: data,
                            list_ticket: data2,
                        });
                });
        });
});



// SAMPLE
router.post('/test-xendit-with-promise', (req, res) => {

    const x = new Xendit({
        secretKey: secret_key,
    });
    const { Invoice } = x;
    const invoiceSpecificOptions = {};
    const i = new Invoice(invoiceSpecificOptions);

    i.createInvoice({
        externalID: Date.now().toString(),
        payerEmail: 'example@gmail.com',
        description: 'Invoice for Shoes Purchase',
        amount: 10000,
    })
        .then(r => {
            // console.log('created invoice', r); // eslint-disable-line no-console
            res.json({
                message: 'xendit ok',
                invoice_id: r.id,
                invoice_url: r.invoice_url
            })
            return r;
        })
        // .then(r => i.getInvoice({ invoiceID: r.id }))
        // .then(r => {
        //     console.log('retrieved invoice', r); // eslint-disable-line no-console
        //     return r;
        // })
        // .then(r => i.expireInvoice({ invoiceID: r.id }))
        // .then(r => {
        //     console.log('expired invoice', r); // eslint-disable-line no-console
        //     return r;
        // })
        // .then(() => i.getAllInvoices())
        // .then(r => {
        //     console.log('first 10 invoices', r); // eslint-disable-line no-console
        //     return r;
        // })
        .catch(e => {
            console.error(e); // eslint-disable-line no-console
            process.exit(1);
        });

});
router.post('/test-xendit', (req, res) => {
    const x = new Xendit({
        secretKey: secret_key,
    });
    const { Invoice } = x;
    const i = new Invoice({});

    (async function() {
        try {
            let invoice = await i.createInvoice({
                externalID: Date.now().toString(),
                payerEmail: 'example@gmail.com',
                description: 'Invoice for Shoes Purchase',
                amount: 100000,
            });
            console.log('created invoice', invoice); // eslint-disable-line no-console

            // const retrievedInvoice = await i.getInvoice({ invoiceID: invoice.id });
            // // eslint-disable-next-line no-console
            // console.log('retrieved invoice', retrievedInvoice);
            //
            // const expiredInvoice = await i.expireInvoice({
            //     invoiceID: retrievedInvoice.id,
            // });
            // // eslint-disable-next-line no-console
            // console.log('expired invoice', expiredInvoice);
            //
            // const invoices = await i.getAllInvoices();
            // // eslint-disable-next-line no-console
            // console.log('first 10 invoices', invoices);

            process.exit(0);
        } catch (e) {
            console.error(e); // eslint-disable-line no-console
            process.exit(1);
        }
    })();
});

router.get('/get-invoice', (req, res) => {
    const Xendit = require('xendit-node');
    const x = new Xendit({
        secretKey: secret_key,
    });
    const { Invoice } = x;
    const invoiceSpecificOptions = {};
    const i = new Invoice(invoiceSpecificOptions);

    i.createInvoice({
        externalID: Date.now().toString(),
        payerEmail: 'example@gmail.com',
        description: 'Invoice for Shoes Purchase',
        amount: 100000,
    })
        .then(r => {
            console.log('created invoice', r); // eslint-disable-line no-console
            return r;
        })
        // .then(r => i.getInvoice({ invoiceID: r.id }))
        // .then(r => {
        //     console.log('retrieved invoice', r); // eslint-disable-line no-console
        //     return r;
        // })
        // .then(r => i.expireInvoice({ invoiceID: r.id }))
        // .then(r => {
        //     console.log('expired invoice', r); // eslint-disable-line no-console
        //     return r;
        // })
        // .then(() => i.getAllInvoices())
        // .then(r => {
        //     console.log('first 10 invoices', r); // eslint-disable-line no-console
        //     return r;
        // })
        .catch(e => {
            console.error(e); // eslint-disable-line no-console
            process.exit(1);
        });

    res.json({
        message: 'xendit ok'
    })
});

ticketTypeId = (ticket_type) => {
    switch (ticket_type) {
        case 'individual':
            return 10;
        case 'group':
            return 11;
        default:
            console.log('not found id ticket')
            return '';
    }
}
xenditOrder = (data) => {
    const x = new Xendit({
        secretKey: secret_key,
    });
    const { Invoice } = x;
    const i = new Invoice({});

    (async function() {
        try {
            let invoice = await i.createInvoice({
                externalID: data.invoice_no,
                payerEmail: data.email,
                description: 'Invoice for Shoes Purchase',
                amount: data.price,
            });
            // console.log('created invoice', invoice); // eslint-disable-line no-console
            return invoice
            // const retrievedInvoice = await i.getInvoice({ invoiceID: invoice.id });
            // // eslint-disable-next-line no-console
            // console.log('retrieved invoice', retrievedInvoice);
            //
            // const expiredInvoice = await i.expireInvoice({
            //     invoiceID: retrievedInvoice.id,
            // });
            // // eslint-disable-next-line no-console
            // console.log('expired invoice', expiredInvoice);
            //
            // const invoices = await i.getAllInvoices();
            // // eslint-disable-next-line no-console
            // console.log('first 10 invoices', invoices);

            process.exit(0);
        } catch (e) {
            console.error(e); // eslint-disable-line no-console
            process.exit(1);
        }
    })();
}

randomNumber = (length) => {
   return randomstring.generate({
        length: length,
        charset: 'numeric'
    });
}

module.exports = router;

