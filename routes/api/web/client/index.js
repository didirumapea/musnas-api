const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const jwt = require('jsonwebtoken')
// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 11;
const checkAuth = require('../../../../middleware/check-auth')
// const date = require('../../../../plugins/moment-date-format')
// // const firedb = require('../../../../plugins/firebase')
// const cryptoRandomString = require('crypto-random-string');
// const CryptoJS = require("crypto-js");
// const shortUrl = require('node-url-shortener');
// const path = require('path');
// const readXlsxFile = require('read-excel-file/node');

router.post('/login',  (req, res) => {
    // let page = req.params.page;
    let email = req.body.email
    let password = req.body.password
    // console.log(req.body)

    db.select(
        'id',
        'name',
        'email',
        'phone',
        'address',
        'city',
        'province',
        'password'
    )
        .from('client')
        .where('email', '=', email)
        .andWhere('isDeleted', '0')
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                bcrypt.compare(password, data[0].password).then((result) => {
                    if (result){
                        // delete data[0]['password']
                        // console.log(data)
                        const client = {
                            id: data[0].id,
                            name: data[0].name,
                            email: data[0].email,
                            address: data[0].address,
                            phone: data[0].phone,
                            city: data[0].city,
                            province: data[0].province,
                        }
                        jwt.sign(client, config.jwtSecretKeyClient, {expiresIn: '30d'}, (err, token) => {
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            client.token = token
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: client
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });


            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })

});

router.post('/register', (req, res) => {
    // let account_type = req.body.account_type
    let detailsRegister = {
        fid_event_group: req.body.fid_event_group,
        email: req.body.email,
        password: req.body.password,
        name: req.body.name,
        address: req.body.address,
        phone: req.body.phone,
        city: req.body.city,
        province: req.body.province,
    }

    // let gender = req.body.gender;
    // let provider = req.body.provider
    // console.log(req.body)
    db.select(
        'id',
        'name',
        'email',
    )
        .from('client')
        .where('email', '=', detailsRegister.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // check REF Code IF Exist

            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(detailsRegister.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                        // Store hash in your password DB.
                        detailsRegister.password = hash
                        db('client')
                            .insert(detailsRegister)
                            .then(data2 => {
                                // console.log(data2[0])
                                // console.log(data[0])
                                // Mock User
                                const user = {
                                    id: data2[0],
                                    name: detailsRegister.name,
                                    email: detailsRegister.email,
                                    phone: req.body.phone,
                                    address: req.body.address,
                                    city: detailsRegister.city,
                                }
                                jwt.sign(user, config.jwtSecretKeyClient, {expiresIn: '30d'}, (err, token) => {
                                    // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                    user.token = token
                                    res.json({
                                        success: true,
                                        message: "Register client & Login Client Success!",
                                        // count: data3.length,
                                        // current_page: paginator.current_page,
                                        // limit: paginator.data.length,
                                        // sortBy: sortBy,
                                        data: user
                                        // token: token
                                    });
                                });
                            });
                        // })
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

module.exports = router;