const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const randomstring = require("randomstring");
const Xendit = require('xendit-node');
const mailing = require('../../../../plugins/mailing')
const secret_key = 'xnd_development_DiNPImnSggXO94ts2A8Ud8IqDtRoyzPxytkip5kmlmCchRtJmyAWtzV6hBb7T' // Dev
// const secret_key = 'xnd_production_6YKZaM4xMAI0xONEg6h72OfkelfJcrSOQNY1jdUA3SjMi35TfQrQn5DnMftt4D' // live
//

router.get('/invoice-no=:invoice_no', (req, res) => {
    db.select(
        '*'
    )
        .from('order')
        .where('invoice_no', req.params.invoice_no)
        .paginate(1, 1, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "Get order by id success",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                // sortBy: sortBy,
                data: paginator.data,
            });
        });
});

router.get('/test-email', (req, res) => {
    // console.log(process.env.WEB_URL+':'+process.env.WEB_PORT)
    mailing.sendEmail({
        template: 'payment',
        to: 'didirumapea@gmail.com',
        // to: ['emiriafikiputri@gmail.com', 'didirumapea@gmail.com'],
        // to: 'didirumapea@gmail.com',
        from_aliases: 'Museum Nasional Indonesia',
        from: 'info@museumnasional.or.id',
        subject: `New invoice Pending from Museum Nasional Indonesia`,
        context: {
            // title: title,
            name: 'Didi Kurnia Rumapea',
            // module_id: module_id,
            inv_url: process.env.WEB_URL+':'+process.env.WEB_PORT
        }
    })
   res.json({
       status: true,
       message: 'good'
   })
});

module.exports = router;

