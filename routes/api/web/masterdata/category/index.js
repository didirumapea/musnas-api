const express = require('express');
const router = express.Router();
const db = require('../../../../../database').db; // as const knex = require('knex')(config);
// const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
// const checkAuth = require('../../../../../middleware/check-auth')
// const randomstring = require('randomstring')
// const date = require('../../../../../plugins/moment-date-format')
// const config = require('../../../../../config')
const changeCase = require('change-case')
const slugify = require('slugify')
setupPaginator(db);


router.get('/', (req, res) => {
    // console.log(date.utc())
    res.send('We Are In MASTERDATA WEB CATEGORY ')
})

// GET LIST
router.get('/list/ticket-type=:ticket_type/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*'
    )
        .from('category as t1')
        .innerJoin('ticket_type as t2', 't2.id', 't1.ticket_type_id')
        .where('t2.name', req.params.ticket_type)
        .orderBy(req.params.col_sort, req.params.sortBy)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "Get list country success",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                // sortBy: sortBy,
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('master_province')
        .whereRaw(`CONCAT_WS('', name) LIKE ?`, [`%${req.params.value}%`])
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get province' : 'There are no province yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET LIST BY ID
router.get('/list/id=:province_id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*'
    )
        .from('master_province')
        .where({'id': req.params.province_id})
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: paginator.data.length === 0 ? "Banner masih kosong." : 'Success get data banner',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// UPDATE
router.post('/update', (req, res) =>  {
    req.body.name = changeCase.upperCase(req.body.name)
    req.body.slug = slugify(req.body.name, {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    db.select(
        'id',
        'name'
    )
        .from('master_province')
        .where({
            name: req.body.name,
        })
        .andWhere('id', '<>', req.body.id)
        .then(data2 => {
            if (data2.length === 0){
                db('master_province')
                    .where('id', req.body.id)
                    .update(req.body)
                    .then(data => {
                        res.json({
                            success: true,
                            message: "Update province success.",
                            count: data.length,
                            data: data,
                        });
                    })
            }else{
                res.json({
                    success: false,
                    message: 'Province already exist',
                    // count: data,
                    // data: result,
                });
            }
            return null
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update province failed.",
                // count: data.length,
                data: err,
            });
        });
});
// DELETE
router.delete('/delete/:id', (req, res) => {

    db('master_province').where({ id: req.params.id }).del().then((data) => {
        res.json({
            success: true,
            message: "Delete province success",
            data: data,
        });
    });
});

module.exports = router;