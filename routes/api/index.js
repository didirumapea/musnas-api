const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth')
const checkAuthTechnician = require('../../middleware/check-auth-technician-user')

// ----- WEB

const images_token = require('./images-token');
router.use('/images', images_token);

// ----- WEB

const web_invoice = require('./web/invoice');
const web_customer = require('./web/customer');
const web_masterdata_country = require('./web/masterdata/country');
const web_masterdata_category = require('./web/masterdata/category');
// const web_event = require('./web/event');
const web_client = require('./web/client');
// const web_example = require('./web/example');
const web_users = require('./web/users');
router.use('/web/invoice', web_invoice);
router.use('/web/customer', web_customer);
router.use('/web/masterdata/country', web_masterdata_country);
router.use('/web/masterdata/category', web_masterdata_category);
// router.use('/web/event', web_event);
router.use('/web/client', web_client);
// router.use('/web/example', web_example);
router.use('/web/users', web_users);




// ------------------------------------------------- CMS -----------------------------------------------
const cms_order = require('./cms/order');
const cms_users = require('./cms/users');
const cms_users_admin = require('./cms/users/admin');
const cms_users_technician_user = require('./cms/users/technician-user');
const cms_ticket = require('./cms/ticket');
const cms_auth = require('./cms/auth');
const cms_auth_technician_user = require('./cms/auth/technician-user');
const cms_auth_admin = require('./cms/auth/admin');
const cms_xendit = require('./cms/xendit');
router.use('/cms/order', cms_order);
router.use('/cms/users', cms_users);
router.use('/cms/users/admin', cms_users_admin);
router.use('/cms/users/technician-user', cms_users_technician_user);
router.use('/cms/ticket', cms_ticket);
router.use('/cms/auth', cms_auth);
router.use('/cms/auth/technician-user', cms_auth_technician_user);
router.use('/cms/auth/admin', cms_auth_admin);
router.use('/cms/xendit', cms_xendit);

// --- -------------------------------------------- APPS -----------------------------------------------
const apps_auth = require('./apps/auth');
const apps_auth_technician_user = require('./apps/auth/technician-user');
const apps_user_technician_user = require('./apps/users/technician-user');
const apps_user = require('./apps/users');
// USERS
router.use('/apps/users', apps_user);
router.use('/apps/users/technician-user', apps_user_technician_user);
// AUTH
router.use('/apps/auth/', apps_auth);
router.use('/apps/auth/technician-user', apps_auth_technician_user);




module.exports = router;