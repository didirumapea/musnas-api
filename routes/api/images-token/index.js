const express = require('express');
const router = express.Router();
const db = require('../../../database').db; // as const knex = require('knex')(config);
const config = require('../../../config')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const jwt = require('jsonwebtoken')
const checkImageToken = require('../../../middleware/check-image-token')

router.get('/', (req, res) => {
    res.send('We Are In Images Token Route')
})

router.get('/:subpath/:filename', checkImageToken, (req, res) => {
    console.log(req.userData)
    res.statusCode = 302;
    res.setHeader("Location", `https://cdn.theinviteme.com/assets/images/${req.params.subpath}/${req.params.filename}`)
    res.end();
});

router.get('/generate-token',  (req, res) => {
    let payload = {
        name: 'didi',
        email: 'didirumapea@gmail.com',
        phone: '08128918921',
    }
    jwt.sign(payload, config.jwtSecretKeyImagesToken, {expiresIn: '300s'}, (err, token) => {
        payload.token = token
        res.json({
            success: true,
            message: "generate image token success",
            data: payload
        });
    });
});


module.exports = router;