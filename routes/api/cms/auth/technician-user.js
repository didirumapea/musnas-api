const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config')
const setupPaginator = require('knex-paginator');
const checkAuthTechnician = require('../../../../middleware/check-auth-technician-user')
const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';
// const someOtherPlaintextPassword = 'not_bacon';
setupPaginator(db);

router.get('/', (req, res) => {
    res.send('We Are In Auth Technician Users Route')
})

router.post('/login',  (req, res) => {
    // let page = req.params.page;
    let email = req.body.email
    let password = req.body.password
    // console.log(req.body)

    db.select(
        't1.id',
        'nik',
        't1.name',
        'email',
        't1.phone',
        't1.address',
        't1.tech_type',
        'password',
        't2.city',
        't2.id as service_center_id',
        't2.name as service_center'
        )
        .from('technician as t1')
        .where('email', '=', email)
        .andWhere('t1.isDeleted', '0')
        .innerJoin('service_center as t2', 't1.service_center_id', 't2.id')
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                bcrypt.compare(password, data[0].password).then((result) => {
                    if (result){
                        // delete data[0]['password']
                        // console.log(data)
                        const user = {
                            id: data[0].id,
                            nik: data[0].nik,
                            name: data[0].name,
                            email: data[0].email,
                            address: data[0].address,
                            phone: data[0].phone,
                            city: data[0].city,
                            tech_type: data[0].tech_type,
                            service_center_id: data[0].service_center_id,
                            service_center: data[0].service_center
                            // image: data[0].image,
                            // startDate: data[0].start_date,
                            // endDate: data[0].end_date,
                            // status: data[0].status,
                            // provider: data[0].provider
                        }
                        jwt.sign(user, config.jwtSecretKeyTechnician, {expiresIn: '365d'}, (err, token) => {
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            user.token = token
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: user
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });


            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })

});

router.post('/register', (req, res) => {
    db.select(
        'id',
        'name',
        'email',
    )
        .from('technician_user')
        .where('email', req.body.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // check REF Code IF Exist
            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                            // Store hash in your password DB.
                            req.body.password = hash
                            db('technician_user')
                                .insert(req.body)
                                .then(data2 => {
                                    // console.log(data2[0])
                                    // console.log(data[0])
                                    // Mock User
                                    const user = {
                                        id: data2[0],
                                        account_type_id: req.body.account_type_id,
                                        nik: req.body.nik,
                                        nip: req.body.nip,
                                        name: req.body.name,
                                        email: req.body.email,
                                        phone: req.body.phone,
                                        address: req.body.address,
                                    }
                                    jwt.sign(user, config.jwtSecretKeyTechnicianUser, {expiresIn: '9999 years'}, (err, token) => {
                                        // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                        user.token = token
                                        res.json({
                                            success: true,
                                            message: "Daftar berhasil dan login sukses",
                                            // count: data3.length,
                                            // current_page: paginator.current_page,
                                            // limit: paginator.data.length,
                                            // sortBy: sortBy,
                                            data: user
                                            // token: token
                                        });
                                    });
                                });
                        // })
                    });
                });


            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

router.get('/get/user', checkAuthTechnician,  (req, res) => {
    // let page = req.params.page;
    // let id = req.body.id
    // let remember_token = req.body.remember_token
    // console.log(req.userData)
    db.select(
        't1.id',
        'name',
        'email',
        'image',
        'start_date',
        'end_date',
        'status',
        'remember_token',
        'provider'
        )
        .from('users as t1')
        .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('t1.id', '=', req.userData.id)
        // .andWhere('remember_token', '=', remember_token)
        .orderBy('end_date', 'desc')
        .limit(1)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });

});

async function checkRefCode(callback){
    let i = 0;
    let rows;
    do {
        let random = randomstring.generate({
            length: 6,
            capitalization: 'uppercase',
            charset: 'hex'
        });
        rows = await db.select('*')
            .from('members')
            .where('ref_code', random);

        console.log(rows.length, i, random)
        i++;
        if (rows.length === 0){
            callback(random)
            console.log('refcode belum terdaftar')
        }else{
            // console.log('refcode sudah terdaftar')
        }

        // batasan   loop
        // if (i > 5){
        //     rows.length = 1;
        // }
    }
    while (rows.length !== 0);
    // console.log('its going end')
    // console.log(rows.length)
}

module.exports = router;