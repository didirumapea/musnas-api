const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const date = require('../../../../plugins/moment-date-format')
const checkAuthAdmin = require('../../../../middleware/check-auth-admin')
const slugify = require('slugify')
const path = require('path')
const multer  = require('multer')
const randomstring = require("randomstring");
const changeCase = require('change-case')
const CryptoJS = require("crypto-js");
const genXlsx = require('../../../../plugins/generate-xlsx')
// const storage = multer.diskStorage(
//     {
//         // destination: '/mnt/cdn/bumikita-files/assets/images/content',
//         destination: function (req, file, cb) {
//             // console.log(file)
//             if (file.fieldname === 'file'){
//                 if (file.mimetype.includes('pdf')){
//                     cb(null, '/mnt/cdn/bumikita-files/assets/images/content/baca-yuk/pdf');
//                 }else if(file.mimetype.includes('audio')){
//                     cb(null, '/mnt/cdn/bumikita-files/assets/images/content');
//                 }else{
//                     cb(null, '/mnt/cdn/bumikita-files/assets/images/content');
//                 }
//             }else{
//                 cb(null, '/mnt/cdn/bumikita-files/assets/images/content');
//             }
//             // console.log(file)
//         },
//         filename: function (req, file, cb ) {
//             let image_name = ''
//             // ADD NEW
//             if (req.body.image_name === undefined){
//                 if (req.body.pdf_url === 'null' || req.body.pdf_url === '' || req.body.pdf_url === null || req.body.pdf_url === undefined){
//                     image_name = randomstring.generate({
//                         length: 20,
//                         // capitalization: 'uppercase',
//                     });
//                     cb( null, image_name+`${path.extname(file.originalname)}`);
//                 }else{
//                     image_name = req.body.pdf_url
//                     cb( null, image_name);
//                 }
//             // UPDATED
//             }else{
//                 if (req.body.pdf_url){
//                     image_name = req.body.pdf_url
//                     cb( null, image_name);
//                 }else{
//                     image_name = req.body.image_name.replace(/\.[^/.]+$/, "")
//                     cb( null, image_name+`${path.extname(file.originalname)}`);
//                 }
//                 // image_name = req.body.image_name.replace(/\.[^/.]+$/, "")
//
//             }
//             // console.log(file.mimetype.split('/')[1])
//             // console.log(path.extname(file.originalname))
//             //req.body is empty...
//             //How could I get the new_file_name property sent from client here?
//             // cb( null, random+`${path.extname(file.originalname)}`);
//             // cb( null, image_name+`${path.extname(file.originalname)}`);
//         }
//     }
// );

// let upload = multer({ storage: storage })

router.get('/', (req, res) => {
    res.send('We Are In TICKET Route')
})
// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    db.select(
't1.id',
        't1.id',
        't1.ticket_no',
        't1.order_no',
        't1.in_time',
        't1.out_time',
        't2.name as category_name',
        't1.created_at',
        't1.created_at',
    )
        .from('ticket as t1')
        .innerJoin('category as t2', 't1.category_id', 't2.id')
        // .where(listWhere)
        .orderBy(`t1.${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: 'Ticket still empty',
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data ticket",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });

});
// GET BY ID
router.get('/list/ticket-id=:ticket_id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.id',
        't1.id',
        't1.ticket_no',
        't1.order_no',
        't1.in_time',
        't1.out_time',
        't2.name as category_name',
        't1.created_at',
    )
        .from('ticket as t1')
        .innerJoin('category as t2', 't1.category_id', 't2.id')
        .where({'t1.id': req.params.ticket_id})
        .orderBy('t1.id', req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Ticket Still empty",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data ticket",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    db.select(
        't1.id',
        't1.id',
        't1.ticket_no',
        't1.order_no',
        't1.in_time',
        't1.out_time',
        't2.name as category_name',
        't1.created_at',
    )
        .from('ticket as t1')
        .innerJoin('category as t2', 't1.category_id', 't2.id')
        .whereRaw(`CONCAT_WS('', t1.ticket_no, t1.order_no, t2.name, t1.in_time) LIKE ?`, [`%${req.params.value}%`])
        .orderBy('t1.id', req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data ticket",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sortBy,
                data: paginator.data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get list ticket failed.",
                data: err,
            });
            console.log('ticket : '+err)
        });
});
// GET LIST COLUMN FILTER
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort/column-filter=:column_filter', (req, res) => {
    let rawParse = {}
    if (req.params.column_filter !== 'null'){
        // decrypt
        let reb64 = CryptoJS.enc.Hex.parse(req.params.column_filter);
        let bytes = reb64.toString(CryptoJS.enc.Base64);
        let decrypt = CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
        rawParse = JSON.parse(decrypt.toString(CryptoJS.enc.Utf8))
        // var plain = decrypt.toString(CryptoJS.enc.Utf8);
    }
    let whereVar = {
        't1.is_deleted' : 0,
        't2.name' : rawParse.category_name,
        created_at: rawParse.created_at
    }
    Object.keys(whereVar).forEach((key) => (whereVar[key] === '' || whereVar[key] === undefined) && delete whereVar[key]);
    // console.log(whereVar)
    // const entries = Object.entries(whereVar)

    const q = db.select(
        't1.id',
        't1.id',
        't1.ticket_no',
        't1.order_no',
        't1.in_time',
        't1.out_time',
        't2.name as category_name',
        't1.created_at',
    )
        .from('ticket as t1')
        .innerJoin('category as t2', 't1.category_id', 't2.id');
            if (whereVar.created_at !== undefined){
                if (whereVar.created_at.includes('to')){
                    const from = whereVar.created_at.split('to')[0]
                    const to = whereVar.created_at.split('to')[1]
                    q.whereBetween('t1.created_at', [from, to])
                    delete whereVar.created_at
                }else{
                    q.whereRaw(`CONCAT_WS('', t1.created_at) LIKE ?`, [`%${whereVar.created_at}%`])
                    delete whereVar.created_at
                }
            }
        q.andWhere(whereVar)
        q.orderBy(`${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data ticket",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sort,
                data: paginator.data,
            });
        });
});
// GENERATE XLSX
router.get('/xlsx-create', (req, res) => {
    db.select(
        '*'
    )
        .from('order')
        // .where(listWhere)
        // .orderBy('id', sort)
        // .paginate(limit, page, true)
        .then(data => {
            console.log(data)
            // genXlsx.TicketGenerateXlsx(data, res).then(r => {
            //     console.log('done generate list pre register user')
            // })
            res.json({
                success: true,
                data: data
            })
        });
});

//

// FORCE DELETE
router.delete('/delete/:id', (req, res) => {
    db('content')
        .where({ id: req.params.id })
        .del()
        .then((data) => {
            res.json({
                success: true,
                message: "Sukses delete content",
                data: data,
            });
        });
});

module.exports = router;