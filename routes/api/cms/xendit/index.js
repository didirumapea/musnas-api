const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const randomstring = require("randomstring");
const checkAuthAdmin = require('../../../../middleware/check-auth-admin')
const fetch = require('node-fetch');
const path = require('path');
const mailing = require('../../../../plugins/mailing')

router.get('/', (req, res) => {
    res.send('We Are In TECHNICIAN USER Route')
})


// GET CALLBACK NOTIFICATIONS
router.post('/invoice/notification', (req, res) => {
   console.log(req.body.status)
    if (req.body.external_id !== 'invoice_123124123'){
        if (req.body.status.toLowerCase() === 'paid'){
            mailing.sendEmail({
                template: 'payment',
                to: req.body.payer_email,
                // to: ['emiriafikiputri@gmail.com', 'didirumapea@gmail.com'],
                // to: 'didirumapea@gmail.com',
                from_aliases: 'Museum Nasional Indonesia',
                from: 'info@museumnasional.or.id',
                subject: `Paid invoice from Museum Nasional Indonesia`,
                context: {
                    // title: title,
                    name: req.body.payer_email,
                    // module_id: module_id,
                    inv_url: process.env.WEB_URL,
                    ext_id: req.body.external_id
                }
            })
        }
        db('order')
            .where('order_no', req.body.external_id)
            .update({payment_status: req.body.status.toLowerCase()})
            .then(data => {
                res.json({
                    success: true,
                    message: "Update payment status order success.",
                    // count: data.length,
                    data: data,
                });
            })
    }else{
        res.json({
            success: true,
            message: "Test CALLBACK Success!",
        });
    }
});

// GET CALLBACK NOTIFICATIONS
router.post('/invoice/test-notification', (req, res) => {
    console.log(req.body)
    res.json({
        message: 'ok',
        data: req.body
    })
});

router.get('/get-images', (req, res) => {

    // res.sendFile('./public/images/logo-full.png');
    // res.sendFile('localhost:2121/musnas-files/assets/images/', 'logo-full.png');
    res.statusCode = 302;
    res.setHeader("Location", "https://cdn.theinviteme.com/assets/images/andrinovel/IHXBNmGJX3ZY3A0N0iX3.png");
    res.end();
});


module.exports = router;

