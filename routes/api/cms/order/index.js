const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const genXlsx = require('../../../../plugins/generate-xlsx')
const CryptoJS = require("crypto-js");
// const storage = multer.diskStorage(
//     {
//         // destination: '/mnt/cdn/bumikita-files/assets/images/content',
//         destination: function (req, file, cb) {
//             // console.log(file)
//             if (file.fieldname === 'file'){
//                 if (file.mimetype.includes('pdf')){
//                     cb(null, '/mnt/cdn/bumikita-files/assets/images/content/baca-yuk/pdf');
//                 }else if(file.mimetype.includes('audio')){
//                     cb(null, '/mnt/cdn/bumikita-files/assets/images/content');
//                 }else{
//                     cb(null, '/mnt/cdn/bumikita-files/assets/images/content');
//                 }
//             }else{
//                 cb(null, '/mnt/cdn/bumikita-files/assets/images/content');
//             }
//             // console.log(file)
//         },
//         filename: function (req, file, cb ) {
//             let image_name = ''
//             // ADD NEW
//             if (req.body.image_name === undefined){
//                 if (req.body.pdf_url === 'null' || req.body.pdf_url === '' || req.body.pdf_url === null || req.body.pdf_url === undefined){
//                     image_name = randomstring.generate({
//                         length: 20,
//                         // capitalization: 'uppercase',
//                     });
//                     cb( null, image_name+`${path.extname(file.originalname)}`);
//                 }else{
//                     image_name = req.body.pdf_url
//                     cb( null, image_name);
//                 }
//             // UPDATED
//             }else{
//                 if (req.body.pdf_url){
//                     image_name = req.body.pdf_url
//                     cb( null, image_name);
//                 }else{
//                     image_name = req.body.image_name.replace(/\.[^/.]+$/, "")
//                     cb( null, image_name+`${path.extname(file.originalname)}`);
//                 }
//                 // image_name = req.body.image_name.replace(/\.[^/.]+$/, "")
//
//             }
//             // console.log(file.mimetype.split('/')[1])
//             // console.log(path.extname(file.originalname))
//             //req.body is empty...
//             //How could I get the new_file_name property sent from client here?
//             // cb( null, random+`${path.extname(file.originalname)}`);
//             // cb( null, image_name+`${path.extname(file.originalname)}`);
//         }
//     }
// );
//
// let upload = multer({ storage: storage })


router.get('/', (req, res) => {
    res.send('We Are In ORDER Route')
})
// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        '*',
    )
        .from('order as t1')
        // .where(listWhere)
        .orderBy(`t1.${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: 'Ticket still empty',
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data ticket",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});
// GET BY ID
router.get('/list/content-id=:content_id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.user_id',
        't1.id as content_id',
        't1.title as content_title',
        't1.content',
        't1.slug',
        't1.is_verified',
        't1.image as content_image',
        't1.video_url',
        't1.audio_url',
        't1.pdf_url',
        't1.publish_date',
        't1.created_at',
        't1.updated_at',
        't2.id as content_type_id',
        't2.name as content_type_name',
        't3.id as disaster_id',
        't3.name as disaster_name',
        't4.id as article_type_id',
        't4.name as article_type_name',
        't5.name as author',
        't5.img_url as author_image',
        't6.name as author_organization',
        't6.img_url as author_organization_image',
        't7.name as author_type',
    )
        .from('content as t1')
        .innerJoin('master_content_type as t2', 't1.content_type_id', 't2.id')
        .innerJoin('master_disaster as t3', 't1.disaster_id', 't3.id')
        .innerJoin('master_article_type as t4', 't1.article_type_id', 't4.id')
        .innerJoin('users as t5', 't1.user_id', 't5.id')
        .innerJoin('master_organization as t6', 't5.id_organization', 't6.id')
        .innerJoin('master_account_type as t7', 't6.id_account_type', 't7.id')
        .where({'t1.id': req.params.content_id})
        .orderBy('t1.id', req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Content Masih Kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data content",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    db.select(
        '*'
    )
        .from('order as t1')
        .whereRaw(`CONCAT_WS('', t1.invoice_no, t1.booking_no, t1.order_no, t1.name, t1.email) LIKE ?`, [`%${req.params.value}%`])
        .orderBy('t1.id', req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data content",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sortBy,
                data: paginator.data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get list search order failed.",
                data: err,
            });
            console.log('order : '+err)
        });



});
// GET LIST COLUMN FILTER
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort/column-filter=:column_filter', (req, res) => {
    let resultDec = {
        't1.is_deleted': '0',
    }
    if (req.params.column_filter !== 'null'){
        // decrypt
        let reb64 = CryptoJS.enc.Hex.parse(req.params.column_filter);
        let bytes = reb64.toString(CryptoJS.enc.Base64);
        let decrypt = CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
        resultDec = JSON.parse(decrypt.toString(CryptoJS.enc.Utf8))
        // var plain = decrypt.toString(CryptoJS.enc.Utf8);
    }
    const entries = Object.entries(resultDec)
    const q = db.select(
        't1.id',
        't1.id',
        't1.ticket_no',
        't1.order_no',
        't1.in_time',
        't1.out_time',
        't2.name as category_name',
        't1.created_at',
    )
        .from('ticket as t1')
        .innerJoin('category as t2', 't1.category_id', 't2.id');
    if (entries[0][1].includes('to')){
        const from = entries[0][1].split('to')[0]
        const to = entries[0][1].split('to')[1]
        q.whereBetween('t1.created_at', [from, to])
    }else{
        q.whereRaw(`CONCAT_WS('', t1.created_at) LIKE ?`, [`%${entries[0][1]}%`])
    }
    q.orderBy(`${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data ticket",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sort,
                data: paginator.data,
            });
        });
});

router.get('/xlsx-order', (req, res) => {
    db.select(
        't1.order_no',
        't1.agency',
        't1.city',
        't3.name',
    )
        .count('* as total')
        .from('order as t1')
        .innerJoin('ticket as t2', 't1.order_no', 't2.order_no')
        .innerJoin('category as t3', 't2.category_id', 't3.id')
        // .where(listWhere)
        // .orderBy('id', sort)
        // .paginate(limit, page, true)
        .groupBy('t2.category_id', 't2.order_no', 't1.agency', 't1.city')
        .then(data => {
            const arr = groupByKey(data, 'order_no')
            let result = []
            for (let key in arr) {
                if (arr.hasOwnProperty(key)) {
                    // console.log(key + " -> " + arr[key]);
                    let dataObj = {
                        no_order: key
                    }
                    arr[key].forEach(element => {
                        dataObj.agency = element.agency
                        dataObj.city = element.city
                        dataObj[element.name] = element.total
                        // console.log(element.name)
                        // console.log(element.total)
                    })
                    result.push(dataObj)
                }
            }
            // console.log(groupByKey(data, 'order_no'))
            // console.log(result)
            genXlsx.orderGenerateXlsx(result, res).then(r => {
                console.log('done generate list pre register user')
            })
            // res.json({
            //     success: true,
            //     // data: data,
            //     result: result
            // })
        });
});

// FORCE DELETE
router.delete('/delete/:id', (req, res) => {
    db('content')
        .where({ id: req.params.id })
        .del()
        .then((data) => {
            res.json({
                success: true,
                message: "Sukses delete content",
                data: data,
            });
        });
});

function groupByKey(array, key) {
    return array
        .reduce((hash, obj) => {
            if(obj[key] === undefined) return hash;
            return Object.assign(hash, { [obj[key]]:( hash[obj[key]] || [] ).concat(obj)})
        }, {})
}

module.exports = router;