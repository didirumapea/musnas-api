const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format');
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const randomstring = require("randomstring");
const checkAuthAdmin = require('../../../../middleware/check-auth-admin')
const jwt = require('jsonwebtoken');
// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
const config = require('../../../../config');

router.get('/', (req, res) => {
    res.send('We Are In TECHNICIAN USER Route')
})

// GET TECHNICIAN USER
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
        't2.name as account_type_name'
    )
        .from('technician_user as t1')
        .innerJoin('account_type as t2', 't1.account_type_id', 't2.id')
        // .where(listWhere)
        .orderBy(`t1.${req.params.col_sort}`, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Technician user masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data technician user",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// GET BY ID
router.get('/list/id=:id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
            '*'
    )
        .from('users')
        .where({'id': req.params.id})
        .orderBy('id', req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: paginator.data.length !== 0,
                message: paginator.data.length !== 0 ? 'Success get user technician' : 'There are no user technician yet',
                limit: paginator.per_page,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                data: paginator.data,
            });
        });
});
// GET LIST SEARCH
router.get('/list/search-global=:value/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {

    db.select(
        't1.id as content_id',
        't1.title as content_title',
        't1.content as content',
        't1.slug',
        't1.image as content_image',
        't1.publish_date',
        't1.is_deleted',
        't1.is_verified',
        't2.name as content_type_name',
        't3.name as disaster_name',
        't4.name as article_name',
        't5.name as author',
        't5.img_url as author_image',
        't6.name as author_organization',
        't6.img_url as author_organization_image',
        't7.name as author_type',
    )
        .from('content as t1')
        .innerJoin('master_content_type as t2', 't1.content_type_id', 't2.id')
        .innerJoin('master_disaster as t3', 't1.disaster_id', 't3.id')
        .innerJoin('master_article_type as t4', 't1.article_type_id', 't4.id')
        .innerJoin('users as t5', 't1.user_id', 't5.id')
        .innerJoin('master_organization as t6', 't5.id_organization', 't6.id')
        .innerJoin('master_account_type as t7', 't6.id_account_type', 't7.id')
        .whereRaw(`CONCAT_WS('', t1.title, t3.name, t2.name) LIKE ?`, [`%${req.params.value}%`])
        .orderBy('t1.id', req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "sukses ambil data content",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                sortBy: req.params.sortBy,
                data: paginator.data,
            });
        })
        .catch((err) => {
            res.json({
                success: false,
                message: "Get list content failed.",
                data: err,
            });
            console.log('content : '+err)
        });



});

// ADD
router.post('/add', (req, res) => {
    req.body.account_type_id = 5;
    db.select(
        'id',
        'name',
        'email',
    )
        .from('technician_user')
        .where('email', req.body.email)
        .then(data => {
            // check REF Code IF Exist
            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        // Store hash in your password DB.
                        req.body.password = hash
                        db('technician_user')
                            .insert(req.body)
                            .then(data2 => {
                                // Mock User
                                const user = {
                                    id: data2[0],
                                    name: req.body.name,
                                    email: req.body.email,
                                    phone: req.body.phone,
                                }
                                jwt.sign(user, config.jwtSecretKeyTechnicianUser, {expiresIn: '9999 years'}, (err, token) => {
                                    // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                    user.token = token
                                    res.json({
                                        success: true,
                                        message: "Daftar berhasil",
                                        data: user
                                        // token: token
                                    });
                                });
                            });
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })
});

// UPDATE IS ACTIVE
router.post('/update-is-active', (req, res) =>  {
    // console.log(req.body)
    db('content')
        .where('id', req.body.id)
        .update('is_deleted', req.body.is_deleted)
        .then(data => {
            res.json({
                success: true,
                message: "Update content status succedd.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update content status failed",
                // count: data.length,
                data: err,
            });
        });

});

// UPDATE
router.post('/update', checkAuthAdmin, (req, res) => {
    req.body.publish_date = moment(req.body.publish_date).utc().format('YYYY-MM-DD HH:mm:ss')
    req.body.user_id = req.userData.user_id
    req.body.updated_at = date.utc()
    req.body.slug = slugify(req.body.title, {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    req.body.title = changeCase.titleCase(req.body.title)
    if (req.files) {
        // console.log(req.files)
        const entries = Object.entries(req.files)
        // req.body.video_url ? req.body.file_type = 'video' : req.body.file_type = 'text'
        entries.forEach((element) => {
            if (element[0] === 'file'){
                req.body.content = null
                if (element[1][0].mimetype.includes('pdf')){
                    // console.log(element[1][0].filename)
                    req.body.pdf_url === 'null' ? req.body.pdf_url = `${element[1][0].filename}` : ''
                    // req.body.pdf_url = `${element[1][0].filename}`
                    req.body.file_type = 'pdf'
                }else{
                    req.body.audio_url = `${element[1][0].filename}`
                    req.body.file_type = 'audio'
                }
            }else{
                req.body.content_type_id === '7' ? req.body.file_type = 'video' : req.body.file_type = 'text'
                req.body.image = `${element[1][0].filename}`
                delete req.body.file;
                delete req.body.image_name;
                delete req.body.file_name;
            }
        })
        if (req.body.content_type_id === '7'){
            req.body.content = null
            if (req.body.video_url){
                req.body.file_type = 'video'
                req.body.pdf_url = null
            }else{
                req.body.file_type = 'pdf'
                req.body.video_url = null
            }
        }else{
            req.body.file_type = 'text'
            req.body.pdf_url = null
            !req.body.video_url ? req.body.video_url = null : ''
        }
        db.select(
            'title',
        )
            .from('content')
            .where({
                title: req.body.title,
            })
            .then((data) => {
                db('content')
                    .where('id', req.body.id)
                    .update(req.body)
                    .then(data2 => {
                        res.json({
                            success: true,
                            message: "Update content berhasil.",
                            count: data2.length,
                            data: data2,
                        });
                    })
                return null
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Update Content failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "upload photo failed",
        });
    }
});

// UPDATE IS VERIFIED
router.post('/update-is-verified', checkAuthAdmin, (req, res) =>  {
    if (req.body.is_verified === 1){
        exp_trans.setTransExp(db, req.body.user_id, req.body.id, 8, 'Add exp point user post content', 'content')
            .then(r => {
                console.log(r)
            })
    }
    db('content')
        .where('id', req.body.id)
        .update({
            'is_verified': req.body.is_verified,
            updated_at: date.utc()
        })
        .then(data => {
            res.json({
                success: true,
                message: "Update content verified success.",
                count: data.length,
                data: data,
            });
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update content verified failed",
                // count: data.length,
                data: err,
            });
        });

});

// FORCE DELETE
router.delete('/delete/:id', (req, res) => {
    db('content')
        .where({ id: req.params.id })
        .del()
        .then((data) => {
            res.json({
                success: true,
                message: "Sukses delete content",
                data: data,
            });
        });
});

module.exports = router;

