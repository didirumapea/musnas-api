const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment');
const config = require('../../../../config')
const setupPaginator = require('knex-paginator');
const checkAuth = require('../../../../middleware/check-auth')
const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')
const date = require('../../../../plugins/moment-date-format')
const mailing = require('../../../../plugins/mailing')
const cryptoRandomString = require('crypto-random-string');

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';
// const someOtherPlaintextPassword = 'not_bacon';
setupPaginator(db);

let dateNow = moment().format("YYYY-MM-DD hh:mm:ss")

router.get('/', (req, res) => {
    res.send('We Are In Auth User Route')
})

router.post('/user/login',  (req, res) => {
    db.select(
        'id',
        'name',
        'email',
        'password',
        'img_url',
        'phone',
        'social_token',
        'register_type'
        )
        .from('users')
        .where('email', req.body.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // console.log(data)
            if (data.length > 0){
                bcrypt.compare(req.body.password, data[0].password).then((result) => {
                    if (result){
                        const user = {
                            id: data[0].id,
                            name: data[0].name,
                            email: data[0].email,
                            img_url: data[0].img_url,
                            phone: data[0].phone,
                            social_token: data[0].social_token,
                            register_type: data[0].register_type,
                        }
                        jwt.sign(user, config.jwtSecretKey, {expiresIn: '360d'}, (err, token) => {
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            user.token = token
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: user
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });
            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })
});

router.post('/user/login-social',  (req, res) => {

    // EMAIL CHECKER
    db.select(
        'id',
        'email',
        'name',
        'img_url',
        'phone',
        'social_token',
        'register_type',
    )
        .from('users')
        // .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('email', req.body.email)
        // .orderBy('end_date', 'desc')
        // .limit(1)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                // Mock User
                const user = {
                    id: data[0].id,
                    name: data[0].name,
                    email: data[0].email,
                    img_url: data[0].img_url,
                    phone: data[0].phone,
                    social_token: data[0].social_token,
                    register_type: data[0].register_type,
                }
                jwt.sign(user, config.jwtSecretKey, {expiresIn: '360d'}, (err, token) => {
                    user.token = token
                    res.json({
                        success: true,
                        message: "Login social with email succees",
                        count: data.length,
                        data: user
                    });
                });
                // PROVIDER CHECKER
            }else{
                db.select(
                    'id'
                )
                    .from('users')
                    .where('register_type', req.body.register_type)
                    .andWhere('social_token', req.body.social_token)
                    .then((data2) => {
                        if (data2.length > 0){
                            db.select(
                                'id',
                                'email',
                                'name',
                                'img_url',
                                'phone',
                                'social_token',
                                'register_type',
                            )
                                .from('users')
                                // .leftJoin('t_order as t2', 't1.id', 't2.id_user')
                                .where('id', '=', data2[0].id)
                                // .orderBy('end_date', 'desc')
                                .limit(1)
                                .then(data3 => {
                                    // console.log(data)
                                    if (data3.length > 0){
                                        // Mock User
                                        const user = {
                                            id: data3[0].id,
                                            name: data3[0].name,
                                            email: data3[0].email,
                                            img_url: data3[0].img_url,
                                            phone: data3[0].phone,
                                            social_token: data3[0].social_token,
                                            register_type: data3[0].register_type,
                                        }
                                        jwt.sign(user, config.jwtSecretKey, {expiresIn: '360d'}, (err, token) => {
                                            user.token = token
                                            res.json({
                                                success: true,
                                                message: "Login social with social_token sukses",
                                                count: data.length,
                                                data: user
                                            });
                                        });
                                    }else {
                                        res.json({
                                            success: false,
                                            message: "something error",
                                            // count: data.length,
                                            // data: user
                                        });
                                    }

                                });
                        }else{
                            db('users')
                                .insert({
                                    name: req.body.name,
                                    email: req.body.email,
                                    img_url: req.body.img_url,
                                    register_type: req.body.register_type,
                                    social_token: req.body.social_token,
                                    created_at: date.utc(),
                                    // updated_at: date.utc7()
                                })
                                .then(data4 => {
                                    // console.log(data4)
                                    db.select(
                                        'id',
                                        'email',
                                        'name',
                                        'img_url',
                                        'phone',
                                        'social_token',
                                        'register_type',
                                    )
                                        .from('users')
                                        // .leftJoin('t_order as t2', 't1.id', 't2.id_user')
                                        .where('id', '=', data4[0])
                                        // .orderBy('end_date', 'desc')
                                        // .limit(1)
                                        .then(data5 => {
                                            // console.log(data5)
                                            // Mock User
                                            const user = {
                                                id: data5[0].id,
                                                name: data5[0].name,
                                                email: data5[0].email,
                                                img_url: data5[0].img_url,
                                                phone: data5[0].phone,
                                                social_token: data5[0].social_token,
                                                register_type: data5[0].register_type,
                                            }
                                            jwt.sign(user, config.jwtSecretKey, {expiresIn: '360d'}, (err, token) => {
                                                // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                                user.token = token
                                                res.json({
                                                    success: true,
                                                    message: "Daftar social berhasil dan login social sukses",
                                                    count: data5.length,
                                                    data: user
                                                });
                                            });

                                        });
                                });

                        }

                    })
            }
        });





});

router.post('/user/register', (req, res) => {
    // let account_type = req.body.account_type
    let detailsUser = {
         email: req.body.email,
         password: req.body.password,
         name: req.body.name,
         img_url: req.body.img_url,
         phone: req.body.phone ,
         social_token: req.body.social_token,
         register_type: req.body.register_type,
         created_at: date.utc(),
    }

    // let provider = req.body.provider
    // console.log(req.body)
    db.select(
        'id',
        'email',
    )
        .from('users')
        .where('email', '=', detailsUser.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // check REF Code IF Exist

            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(detailsUser.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                            // Store hash in your password DB.
                            detailsUser.password = hash
                            db('users')
                                .insert(detailsUser)
                                .then(data2 => {
                                    // console.log(data2)
                                    // console.log(data[0])
                                    // Mock User
                                    db.select(
                                        'id',
                                        'email',
                                        'name',
                                        'img_url',
                                        'phone',
                                        'social_token',
                                        'register_type'
                                    )
                                        .from('users')
                                        .where('id', data2[0])
                                        .then((data3) => {
                                            //console.log(data3)
                                            const user = {
                                                id: data2[0],
                                                email: data3[0].email,
                                                name: data3[0].name,
                                                img_url: data3[0].img_url,
                                                phone: data3[0].phone ,
                                                social_token: data3[0].social_token,
                                                register_type: data3[0].register_type,
                                                created_at: data3[0].created_at,
                                            }
                                            jwt.sign(user, config.jwtSecretKey, {expiresIn: '360d'}, (err, token) => {
                                                // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                                user.token = token
                                                res.json({
                                                    success: true,
                                                    message: "Daftar berhasil dan login sukses",
                                                    // count: data3.length,
                                                    // current_page: paginator.current_page,
                                                    // limit: paginator.data.length,
                                                    // sortBy: sortBy,
                                                    data: user
                                                    // token: token
                                                });
                                            });
                                        })
                                });
                        // })
                    });
                });
            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

router.get('/get/user', checkAuth,  (req, res) => {
    // let page = req.params.page;
    // let id = req.body.id
    // let remember_token = req.body.remember_token
    // console.log(req.userData)
    db.select(
        't1.id',
        'name',
        'email',
        'image',
        'start_date',
        'end_date',
        'status',
        'remember_token',
        'provider'
        )
        .from('users as t1')
        .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('t1.id', '=', req.userData.id)
        // .andWhere('remember_token', '=', remember_token)
        .orderBy('end_date', 'desc')
        .limit(1)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });

});

// RESET PASSWORD
router.post('/user/reset-password', (req, res) => {
    let email = req.body.email
    db.select(
        'id',
        'name',
        'email',
    )
        .from('users')
        .where('email', email)
        .then(data => {
            if (data.length === 0){
                res.json({
                    // user: req.userData
                    success: false,
                    message: "Email tidak di temukan",
                });
            }else{
                let newPassword = cryptoRandomString({length: 20, type: 'url-safe'});
                // let newPassword = randomstring.generate({
                //     length: 20,
                    // capitalization: 'uppercase',
                    // charset: 'hex'
                // });
                console.log('Auth.js: '+newPassword)
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(newPassword, salt, (err, hash) => {
                        db('users')
                            .where('email', email)
                            .update({password: hash})
                            .then(data2 => {
                                // console.log(data)
                                data[0].password = newPassword
                                mailing.sendEmail(data[0].email, newPassword, 'Request reset password raja repair', 'reset-password-member', data[0].name)
                                mailing.sendEmail(data[0])
                                res.json({
                                    success: true,
                                    message: "Success Change Password User",
                                    // isMatch: isMatch,
                                    password: hash,
                                    // data: data
                                })
                            }).catch((err) =>{
                            console.log(err)
                            res.json({
                                success: false,
                                message: "Reset password failed.",
                                // count: data.length,
                                data: err,
                            });
                        });
                    });
                });
            }
        });
//
});

// CHANGE PASSWORD
router.post('/user/change-password',  (req, res) => {
    // let page = req.params.page;
    let email = req.body.email
    let oldPassword = req.body.old_password
    let newPassword = req.body.new_password
    //console.log(req.body)

    db.select(
        'id',
        'name',
        'email',
        'password',
    )
        .from('users')
        .where('email', email)
        .then(data => {
            // console.log(data)
            if (data.length > 0){
                bcrypt.compare(oldPassword, data[0].password).then((result) => {
                    if (result){
                        bcrypt.genSalt(saltRounds, (err, salt) => {
                            bcrypt.hash(newPassword, salt, (err, hash) => {
                                db('users')
                                    .where('email', email)
                                    .update({password: hash})
                                    .then(data2 => {
                                        // mailing.sendEmailResetPassword(data[0].email, newPassword, 'Request reset password rimember', 'reset-password-member', data[0].fullname)
                                        res.json({
                                            success: true,
                                            message: "Success Change Password User",
                                            // isMatch: isMatch,
                                            password: hash,
                                            // data: data2
                                        })

                                    }).catch((err) =>{
                                    console.log(err)
                                    res.json({
                                        success: false,
                                        message: "Reset password failed.",
                                        // count: data.length,
                                        data: err,
                                    });
                                });
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });


            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })

});

// SAMPLE CODE
router.get('/sample/dowhile', (req, res) => {
    // test(res)
    checkRefCode((response) => {
        console.log(response)
    })
    async function checkRefCode2(){
        let i = 0;
        let rows;
        do {
            let random = randomstring.generate({
                length: 6,
                capitalization: 'uppercase',
                charset: 'hex'
            });
            rows = await db.select('*')
                .from('members')
                .where('ref_code', random);

            console.log(rows.length, i, random)
            i++;
            if (rows.length === 0){
                console.log('refcode belum terdaftar')
            }else{
                console.log('refcode sudah terdaftar')
            }

            // batasan   loop
            // if (i > 5){
            //     rows.length = 1;
            // }
        }
        while (rows.length !== 0);

        // console.log('its going end')
        // console.log(rows.length)
    }
    res.json({
        message: 'Post Created',
        data: 'looking good'
    })

})

async function checkRefCode(callback){
    let i = 0;
    let rows;
    do {
        let random = randomstring.generate({
            length: 6,
            capitalization: 'uppercase',
            charset: 'hex'
        });
        rows = await db.select('*')
            .from('members')
            .where('ref_code', random);

        console.log(rows.length, i, random)
        i++;
        if (rows.length === 0){
            callback(random)
            console.log('refcode belum terdaftar')
        }else{
            // console.log('refcode sudah terdaftar')
        }

        // batasan   loop
        // if (i > 5){
        //     rows.length = 1;
        // }
    }
    while (rows.length !== 0);
    // console.log('its going end')
    // console.log(rows.length)
}

router.post('/sample/jwt/v2', checkAuth, (req, res) => {
    res.json({
        message: 'Post Created',
        data: req.userData
    })
})


module.exports = router;