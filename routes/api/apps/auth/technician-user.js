const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config')
const setupPaginator = require('knex-paginator');
const checkAuthTechnician = require('../../../../middleware/check-auth-technician-user')
const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')

// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 10;
// const myPlaintextPassword = 's0/\/\P4$$w0rD';
// const someOtherPlaintextPassword = 'not_bacon';
setupPaginator(db);



router.get('/', (req, res) => {
    res.send('We Are In Auth Technician USERS Route')
})

router.post('/login',  (req, res) => {
    db.select(
        't1.id',
        't1.account_type_id',
        'nik',
        'nip',
        't1.name',
        'email',
        't1.phone',
        't1.address',
        'password',
        't2.name as account_type_name',
        )
        .from('technician_user as t1')
        .where('email', req.body.email)
        .andWhere('t1.is_deleted', 0)
        .innerJoin('account_type as t2', 't1.account_type_id', 't2.id')
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length > 0){
                bcrypt.compare(req.body.password, data[0].password).then((result) => {
                    if (result){
                        // delete data[0]['password']
                        // console.log(data)
                        const user = {
                            id: data[0].id,
                            account_type_id: data[0].account_type_id,
                            account_type_name: data[0].account_type_name,
                            nik: data[0].nik,
                            nip: data[0].nip,
                            name: data[0].name,
                            email: data[0].email,
                            address: data[0].address,
                            phone: data[0].phone,
                            city: data[0].city,
                        }
                        jwt.sign(user, config.jwtSecretKeyTechnicianUser, {expiresIn: '9999 years'}, (err, token) => {
                            // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                            user.token = token
                            res.json({
                                success: true,
                                message: "Login sucessful",
                                // count: data.length,
                                data: user
                            });
                        });
                    }else{
                        res.json({
                            success: result,
                            message: "Password salah.",
                            // data: data,
                        })
                    }
                    // console.log(res)
                });
            }else {
                res.json({
                    success: false,
                    message: "Email salah atau belum terdaftar",
                    // data: data,
                })
            }
        })

});

router.post('/user/register', (req, res) => {
    // let account_type = req.body.account_type
    let detailsRegister = {
         nik: req.body.nik,
         name: req.body.name,
         email: req.body.email,
         password: req.body.password,
         phone: req.body.phone,
         address: req.body.address,
         service_center_id: req.body.service_center_id,
    }

    // let gender = req.body.gender;
    // let provider = req.body.provider
    // console.log(req.body)
    db.select(
        'id',
        'name',
        'email',
    )
        .from('technician')
        .where('email', '=', detailsRegister.email)
        // .orderBy('position', sortBy)
        // .paginate(limit, page, true)
        .then(data => {
            // check REF Code IF Exist

            if (data.length === 0){
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    bcrypt.hash(detailsRegister.password, salt, (err, hash) => {
                        // checkRefCode((gen_ref_code) => {
                            // Store hash in your password DB.
                            detailsRegister.password = hash
                            db('technician')
                                .insert(detailsRegister)
                                .then(data2 => {
                                    // console.log(data2[0])
                                    // console.log(data[0])
                                    // Mock User
                                    const user = {
                                        id: data2[0],
                                        nik: detailsRegister.nik,
                                        name: detailsRegister.name,
                                        email: detailsRegister.email,
                                        phone: req.body.phone,
                                        address: req.body.address,
                                        service_center_id: req.body.service_center_id,
                                    }
                                    jwt.sign(user, config.jwtSecretKeyUserTechnician, {expiresIn: '9999 years'}, (err, token) => {
                                        // jwt.sign({user}, secretKey, {expiresIn: '30s'}, (err, token) => {
                                        user.token = token
                                        res.json({
                                            success: true,
                                            message: "Daftar berhasil dan login sukses",
                                            // count: data3.length,
                                            // current_page: paginator.current_page,
                                            // limit: paginator.data.length,
                                            // sortBy: sortBy,
                                            data: user
                                            // token: token
                                        });
                                    });
                                });
                        // })
                    });
                });


            }else {
                res.json({
                    success: false,
                    message: "Email sudah terdaftar",
                    // data: data,
                })
            }
        })

});

router.get('/get/user', checkAuthTechnician,  (req, res) => {
    // let page = req.params.page;
    // let id = req.body.id
    // let remember_token = req.body.remember_token
    // console.log(req.userData)
    db.select(
        't1.id',
        'name',
        'email',
        'image',
        'start_date',
        'end_date',
        'status',
        'remember_token',
        'provider'
        )
        .from('users as t1')
        .leftJoin('t_order as t2', 't1.id', 't2.id_user')
        .where('t1.id', '=', req.userData.id)
        // .andWhere('remember_token', '=', remember_token)
        .orderBy('end_date', 'desc')
        .limit(1)
        // .paginate(limit, page, true)
        .then(data => {
            if (data.length === 0){
                res.json({
                    success: false,
                    message: "Data tidak di temukan",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Sukses ambil data user",
                    count: data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: data,
                });
            }
        });

});


// SAMPLE CODE
router.get('/sample/dowhile', (req, res) => {
    // test(res)
    checkRefCode((response) => {
        console.log(response)
    })
    async function checkRefCode2(){
        let i = 0;
        let rows;
        do {
            let random = randomstring.generate({
                length: 6,
                capitalization: 'uppercase',
                charset: 'hex'
            });
            rows = await db.select('*')
                .from('members')
                .where('ref_code', random);

            console.log(rows.length, i, random)
            i++;
            if (rows.length === 0){
                console.log('refcode belum terdaftar')
            }else{
                console.log('refcode sudah terdaftar')
            }

            // batasan   loop
            // if (i > 5){
            //     rows.length = 1;
            // }
        }
        while (rows.length !== 0);

        // console.log('its going end')
        // console.log(rows.length)
    }
    res.json({
        message: 'Post Created',
        data: 'looking good'
    })

})

async function checkRefCode(callback){
    let i = 0;
    let rows;
    do {
        let random = randomstring.generate({
            length: 6,
            capitalization: 'uppercase',
            charset: 'hex'
        });
        rows = await db.select('*')
            .from('members')
            .where('ref_code', random);

        console.log(rows.length, i, random)
        i++;
        if (rows.length === 0){
            callback(random)
            console.log('refcode belum terdaftar')
        }else{
            // console.log('refcode sudah terdaftar')
        }

        // batasan   loop
        // if (i > 5){
        //     rows.length = 1;
        // }
    }
    while (rows.length !== 0);
    // console.log('its going end')
    // console.log(rows.length)
}

module.exports = router;