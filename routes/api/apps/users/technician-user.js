const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const randomstring = require("randomstring");
const checkAuthAdmin = require('../../../../middleware/check-auth-admin')

router.get('/', (req, res) => {
    res.send('We Are In TECHNICIAN USER Route')
})

router.post('/update-in-time-by-ticket-no', (req, res) => {

    db.select(
        'in_time'
    )
        .from('ticket')
        .where('ticket_no', req.body.no_ticket)
        .then(data => {
            if (data[0].in_time === 'Invalid date'){
                db('ticket')
                    .where('ticket_no', req.body.no_ticket)
                    .update({in_time : date.utc()})
                    .then(data2 => {
                        res.json({
                            success: true,
                            message: "Update in time by ticket number success.",
                            data: data2,
                            type_scan: 'ticket',
                        });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({
                            success: false,
                            message: "Update failed.",
                            // count: data.length,
                            data: err,
                        });
                    });
            }else{
                res.json({
                    success: false,
                    message: "The time of entry has been scanned",
                    // data: data2,
                });
            }
        })
});

router.post('/update-out-time-by-ticket-no', (req, res) => {
    db.select(
        'in_time',
        'out_time',
    )
        .from('ticket')
        .where('ticket_no', req.body.no_ticket)
        .then(data => {
            // console.log(data[0].in_time)
            if (data[0].in_time !== 'Invalid date'){
                if (data[0].out_time !== 'Invalid date'){
                    res.json({
                        success: false,
                        message: "Checkout time has been scanned",
                        type_scan: 'ticket',
                    });
                }else{
                    db('ticket')
                        .where('ticket_no', req.body.no_ticket)
                        .update({out_time : date.utc()})
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Update out time by ticket number success.",
                                data: data2,
                                type_scan: 'ticket',
                            });
                        })
                        .catch((err) => {
                            console.log(err)
                            res.json({
                                success: false,
                                message: "Update failed.",
                                // count: data.length,
                                data: err,
                            });
                        });
                }

            }else{
                res.json({
                    success: false,
                    message: "The time of entry has not been scanned",
                    // data: data2,
                });
            }
        })
});

router.post('/update-in-time-by-order-no', (req, res) => {
    db.select(
        'in_time'
    )
        .from('order')
        .where('order_no', req.body.no_order)
        .then(data => {
            if (data[0].in_time === 'Invalid date'){
                db('ticket as t1' )
                    .innerJoin('order as t2', 't1.order_no', 't2.order_no')
                    .where('t2.order_no', req.body.no_order)
                    .update({
                        't2.in_time' : date.utc(),
                        't1.in_time' : date.utc(),
                    })
                    .then(data2 => {
                        res.json({
                            success: true,
                            message: "Update in time by order no success.",
                            data: data2,
                        });
                    })
                    .catch((err) => {
                        console.log(err)
                        res.json({
                            success: false,
                            message: "Update failed.",
                            // count: data.length,
                            data: err,
                        });
                    });
            }else{
                res.json({
                    success: false,
                    message: "The time of entry has been scanned",
                    type_scan: 'order',
                });
            }
        })
});

router.post('/update-out-time-by-order-no', (req, res) => {
    db.select(
        'in_time',
        'out_time',
    )
        .from('order')
        .where('order_no', req.body.no_order)
        .then(data => {
            // console.log(data[0].in_time)
            if (data[0].in_time !== 'Invalid date'){
                if (data[0].out_time !== 'Invalid date'){
                    res.json({
                        success: false,
                        message: "Checkout time has been scanned",
                        type_scan: 'ticket',
                    });
                }else{
                    db('ticket as t1')
                        .innerJoin('order as t2', 't1.order_no', 't2.order_no')
                        .where('t2.order_no', req.body.no_order)
                        .update({
                            't2.out_time' : date.utc(),
                            't1.out_time' : date.utc(),
                        })
                        .then(data2 => {
                            res.json({
                                success: true,
                                message: "Update out time by order number success.",
                                data: data2,
                                type_scan: 'order',
                            });
                        })
                        .catch((err) => {
                            console.log(err)
                            res.json({
                                success: false,
                                message: "Update failed.",
                                // count: data.length,
                                data: err,
                            });
                        });
                }
            }else{
                res.json({
                    success: false,
                    message: "The time of entry has not been scanned",
                    // data: data2,
                });
            }
        })
});

router.get('/key=:key/value=:value', (req, res) => {

    const table_name = req.params.key === 'ticket_no' ? 'ticket' : 'order'

    const q = db.select(
        't1.*'
    )
        .from(table_name + ' as t1');
        if (table_name !== 'ticket'){
            q.innerJoin('ticket as t2', 't1.order_no', 't2.order_no')
            q.count('* as total_ticket')
        }else{
            q.count('* as total_ticket')
            q.groupBy('t1.ticket_no', 't1.id')
        }
        // .innerJoin('ticket as t2', 't1.order_no', 't2.order_no')
        q.where('t1.'+req.params.key, req.params.value)
        .paginate(1, 1, true)
        .then(paginator => {
            res.json({
                success: true,
                message: "Get data success",
                current_page: paginator.current_page,
                limit: paginator.data.length,
                paginate: {
                    totalRow: paginator.total,
                    from: paginator.from,
                    to: paginator.to,
                    currentPage: paginator.current_page,
                    lastPage: paginator.last_page
                },
                // sortBy: sortBy,
                data: paginator.data,
            });
        })
            .catch((err) => {
                res.json({
                    success: false,
                    message: "Get list data failed / key and value not matching",
                    data: err,
                });
                console.log('apps-technician-user : '+err)
            });
});

module.exports = router;

