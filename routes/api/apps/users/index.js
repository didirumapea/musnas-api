const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const date = require('../../../../plugins/moment-date-format')
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const randomstring = require("randomstring");

router.post('/update-in-time', (req, res) => {
    db('ticket')
        .where('ticket_no', req.body.no_ticket)
        .update({in_time : date.utc()})
        .then(data2 => {
            res.json({
                success: true,
                message: "Update in time success.",
                data: data2,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Update failed.",
                // count: data.length,
                data: err,
            });
        });
});

router.post('/update-out-time', (req, res) => {
    db('ticket')
        .where('ticket_no', req.body.no_ticket)
        .update({in_time : date.utc()})
        .then(data2 => {
            res.json({
                success: true,
                message: "Update in time success.",
                data: data2,
            });
        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: "Update failed.",
                // count: data.length,
                data: err,
            });
        });
});
ticketTypeId = (ticket_type) => {
    switch (ticket_type) {
        case 'individual':
            return 10;
        case 'group':
            return 11;
        default:
            console.log('not found id ticket')
            return '';
    }
}

randomNumber = (length) => {
   return randomstring.generate({
        length: length,
        charset: 'numeric'
    });
}

module.exports = router;

